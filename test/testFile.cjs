let {dataBasedOnId, groupingBasedOnCompanies,dataOfParticularCompany,removingDataOfParticularCompany,sortingData,swapingBasedOnPsition,addingBirthDate}=require('../employeeDrillSoluton.cjs');

dataBasedOnId("data.json")
.then((data)=>{
    return groupingBasedOnCompanies("data.json");
})
.then(()=>{
    return dataOfParticularCompany("data.json");
})
.then(()=>{
    return removingDataOfParticularCompany("data.json");
})
.then(()=>{
     return sortingData("data.json");
})
.then(()=>{
    return swapingBasedOnPsition("data.json");
})
.then(()=>{
    return addingBirthDate("data.json");
})
.then((data)=>{
    console.log(data);
})
.catch((error)=>{
    console.log(error);
})